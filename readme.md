If you like to hang out in VRChat in desktop mode while using other apps, you can use this super simple tray app to display your currently active application in the VRChat chatbox.

![image](doc/img/VRChat_1920x1080_2022-10-01_22-17-14.940.png)

You will need to have OSC enabled in VRChat. Download the latest build from the [releases](https://gitea.moe/lamp/VRChat-AFK-Status/releases) page and run the exe. A tray icon will let you know it's running and you can right-click it to stop it.

Every two seconds, it will get the active application and if it's not VRChat, it will send chatbox input to VRChat's default OSC port 9000. Once you're back on VRChat though, it'll clear the chatbox and then you can use it normally.