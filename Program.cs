﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Timers;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Rug.Osc;
using VrcAfkStatus.Properties;

namespace VrcAfkStatus {
	class Program {

		[Flags]
		public enum ProcessAccessFlags : uint {
			PROCESS_ALL_ACCESS = 0x001F0FFF,
			PROCESS_CREATE_PROCESS = 0x00000080,
			PROCESS_CREATE_THREAD = 0x00000002,
			PROCESS_DUP_HANDLE = 0x00000040,
			PROCESS_QUERY_INFORMATION = 0x00000400,
			PROCESS_QUERY_LIMITED_INFORMATION = 0x00001000,
			PROCESS_SET_INFORMATION = 0x00000200,
			PROCESS_SET_QUOTA = 0x00000100,
			PROCESS_SUSPEND_RESUME = 0x00000800,
			PROCESS_TERMINATE = 0x00000001,
			PROCESS_VM_OPERATION = 0x00000008,
			PROCESS_VM_READ = 0x00000010,
			PROCESS_VM_WRITE = 0x00000020
		}

		[DllImport("user32.dll")]
		private static extern IntPtr GetForegroundWindow();

		[DllImport("user32.dll")]
		private static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out IntPtr lpdwProcessId);

		[DllImport("kernel32.dll")]
		private static extern bool QueryFullProcessImageName([In] IntPtr hProcess, [In] uint dwFlags, [Out] StringBuilder lpExeName, ref uint lpdwSize);

		[DllImport("kernel32.dll")]
		private static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, int dwProcessId);

		private static string GetActiveAppName() {
			IntPtr foregroundWindowId = GetForegroundWindow();
			GetWindowThreadProcessId(foregroundWindowId, out IntPtr activeAppProcessId);
			//Process currentAppProcess = Process.GetProcessById((int)activeAppProcessId);
			//string currentAppName = FileVersionInfo.GetVersionInfo(currentAppProcess.MainModule.FileName).FileDescription;
			//return currentAppName;
			IntPtr hprocess = OpenProcess(ProcessAccessFlags.PROCESS_QUERY_LIMITED_INFORMATION, false, (int)activeAppProcessId);
			uint lpdwSize = 1024;
			StringBuilder lpExeName = new StringBuilder((int)lpdwSize);
			QueryFullProcessImageName(hprocess, 0, lpExeName, ref lpdwSize);
			var exePath = lpExeName.ToString();
			//return exePath;
			FileVersionInfo appInfo = FileVersionInfo.GetVersionInfo(exePath);
			return String.IsNullOrEmpty(appInfo.FileDescription) ? Path.GetFileName(exePath) : appInfo.FileDescription;
		}

		private static System.Timers.Timer timer;
		private static OscSender oscsender;

		static void Main(string[] args) {
			Console.WriteLine("owo");

			timer = new System.Timers.Timer(2000);
			timer.Elapsed += OnTimerElapse;
			timer.AutoReset = true;
			timer.Enabled = true;

			oscsender = new OscSender(System.Net.IPAddress.Parse("127.0.0.1"), 9001, 9000);
			oscsender.Connect();
			Application.Run(new TrayApplicationContext());
		}

		private static bool active = false;

		private static void OnTimerElapse(Object source, ElapsedEventArgs e) {
			try {
				string activeAppName = GetActiveAppName();
				Console.WriteLine(activeAppName);
				string chatMessage;
				if (activeAppName == "VRChat.exe") {
					if (active) {
						chatMessage = "";
						active = false;
					} else return;
				} else {
					chatMessage = "AFK: using " + activeAppName;
					active = true;
				};
				oscsender.Send(new OscMessage("/chatbox/input", chatMessage, true));
			} catch (Exception exception) {
				Console.WriteLine(exception);
			}
		}
	}

	class TrayApplicationContext : ApplicationContext {
		private NotifyIcon notifyIcon;
		public TrayApplicationContext() {
			notifyIcon = new NotifyIcon() {
				Icon = Resources.AppIcon,
				ContextMenuStrip = new ContextMenuStrip() {
					Items = { new ToolStripMenuItem("Stop", null, Exit) }
				},
				Visible = true
			};
		}
		void Exit(object sender, EventArgs e) {
			notifyIcon.Visible = false;
			Application.Exit();
		}
	}
}
